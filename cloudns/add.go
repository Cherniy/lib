package cloudns

import "encoding/json"
import (
	"bytes"
	"gitlab.com/Cherniy/lib/error"
	"io/ioutil"
	"net/http"
)

const _cloud = "https://api.cloudns.net/dns/"

const _add = _cloud + "add-record.json"
const _json = "application/json"

func NewRecord(record RecordAdd) []byte {
	data, err := json.Marshal(record)
	error.ErrorLog(error.WARNING, "NewRecord", err)
	response, err := http.Post(_add, _json, bytes.NewBuffer(data))
	error.ErrorLog(error.WARNING, "NewRecord", err)
	defer response.Body.Close()
	resp, err := ioutil.ReadAll(response.Body)
	error.ErrorLog(error.WARNING, "NewRecord", err)
	return resp
}
