package cloudns

var (
	_authId       int
	_authPassword string
)

func SetAuthParams(id int, password string) {
	_authId = id
	_authPassword = password
}

type RecordAdd struct {
	AuthId       int    `json:"auth-id"`
	AuthPassword string `json:"auth-password"`
	DomainName   string `json:"domain-name"`
	RecordType   string `json:"record-type"`
	Host         string `json:"host"`
	Record       string `json:"record"`
	TTL          int    `json:"ttl"`
}
