package error

import (
	"github.com/natefinch/lumberjack"
	"log"
)

func Loger(filename string, mSize, mBacupsFile, mAge int) {
	log.SetOutput(&lumberjack.Logger{
		Filename:   filename + ".log",
		MaxSize:    mSize, // megabytes
		MaxBackups: mBacupsFile,
		MaxAge:     mAge, //days
	})
}
