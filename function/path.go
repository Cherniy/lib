package function

import (
	"log"
	"os"
	"path/filepath"
)

var RealPath string

func init() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Printf("Error, get abs paht : %v", err.Error())
	}
	RealPath = dir + "/"
}
