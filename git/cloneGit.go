package git

import (
	errLib "gitlab.com/Cherniy/lib/error"
	"os/exec"
)

const (
	PULL   = "pull"
	CLONE  = "clone"
	COMMIT = "commit"
	ADD    = "add"
	PUSH   = "push"
	gitrun = "git"
)

func GitExec(comand, path, repository, comment string) {
	var command = func(comand string, args []string) {
		_, err := exec.Command(comand, args...).Output()
		errLib.ErrorLog(errLib.WARNING, "GitExec "+comand, err)
	}
	switch comand {
	case PULL:
		command(gitrun, []string{"-C", path, PULL})
	case COMMIT:
		command(gitrun, []string{"-C", path, COMMIT, "-m", comment})
	case ADD:
		command(gitrun, []string{"-C", path, ADD, "."})
	case CLONE:
		command(gitrun, []string{CLONE, repository, path})
	case PUSH:
		command(gitrun, []string{"-C", path, PUSH})
	}
}
