package basemodule

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/Cherniy/lib/error"
	"gitlab.com/Cherniy/lib/function"
)

var basename string

func SetBaseName(name string) {
	basename = name
}

func connection() *sql.DB {
	db, err := sql.Open("sqlite3", fmt.Sprintf("%v%v.db", function.RealPath, basename))
	error.ErrorLog(error.WARNING, "lib connection()", err)
	return db
}
