package server

import (
	"fmt"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"gitlab.com/Cherniy/lib/function"
	"html/template"
)

var ServerMartini = martini.Classic()

func init() {
	ServerMartini.Use(render.Renderer(render.Options{
		Directory:  fmt.Sprintf("%vtemplate", function.RealPath),
		Layout:     "layout",
		Funcs:      []template.FuncMap{},
		Extensions: []string{".html", ".htm"},
		Charset:    "UTF-8",
		IndentJSON: true,
	}))
	ServerMartini.Use(Middleware)
	staticOptions := martini.StaticOptions{Prefix: "assets"}
	ServerMartini.Use(martini.Static(fmt.Sprintf("%vassets", function.RealPath), staticOptions))
}
